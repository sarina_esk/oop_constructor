#ifndef DEFAULTPROFILE_H
#define DEFAULTPROFILE_H
#include<iostream>
#include<string>
using namespace std;

class Defaultprofile
{
private:
    string first_name;
    string family_name;
public:
   Defaultprofile(string first ,string family);
   string getFirstname();
   string getFamilyname();


};

#endif // DEFAULTPROFILE_H
